﻿namespace KeyworHitDateChanger
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtRecordingID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNewDate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnChangeDate = new System.Windows.Forms.Button();
            this.lstOutput = new System.Windows.Forms.ListBox();
            this.cbxOverwrite = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // txtRecordingID
            // 
            this.txtRecordingID.Location = new System.Drawing.Point(9, 25);
            this.txtRecordingID.Name = "txtRecordingID";
            this.txtRecordingID.Size = new System.Drawing.Size(297, 20);
            this.txtRecordingID.TabIndex = 0;
            this.txtRecordingID.Text = "BCE8E713-2830-D080-8F63-D8E171520001";
            this.txtRecordingID.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "RecordingID:";
            // 
            // txtNewDate
            // 
            this.txtNewDate.Location = new System.Drawing.Point(9, 64);
            this.txtNewDate.Name = "txtNewDate";
            this.txtNewDate.Size = new System.Drawing.Size(143, 20);
            this.txtNewDate.TabIndex = 2;
            this.txtNewDate.Text = "2014-06-15";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "New Date:";
            // 
            // btnChangeDate
            // 
            this.btnChangeDate.Location = new System.Drawing.Point(32, 192);
            this.btnChangeDate.Name = "btnChangeDate";
            this.btnChangeDate.Size = new System.Drawing.Size(120, 45);
            this.btnChangeDate.TabIndex = 4;
            this.btnChangeDate.Text = "Change Date";
            this.btnChangeDate.UseVisualStyleBackColor = true;
            this.btnChangeDate.Click += new System.EventHandler(this.btnChangeDate_Click);
            // 
            // lstOutput
            // 
            this.lstOutput.FormattingEnabled = true;
            this.lstOutput.Location = new System.Drawing.Point(186, 64);
            this.lstOutput.Name = "lstOutput";
            this.lstOutput.Size = new System.Drawing.Size(420, 173);
            this.lstOutput.TabIndex = 5;
            // 
            // cbxOverwrite
            // 
            this.cbxOverwrite.AutoSize = true;
            this.cbxOverwrite.Location = new System.Drawing.Point(49, 114);
            this.cbxOverwrite.Name = "cbxOverwrite";
            this.cbxOverwrite.Size = new System.Drawing.Size(103, 17);
            this.cbxOverwrite.TabIndex = 6;
            this.cbxOverwrite.Text = "Overwrite Date?";
            this.cbxOverwrite.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbxOverwrite.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 251);
            this.Controls.Add(this.cbxOverwrite);
            this.Controls.Add(this.lstOutput);
            this.Controls.Add(this.btnChangeDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNewDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtRecordingID);
            this.Name = "Form1";
            this.Text = "Recording Date Changer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRecordingID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNewDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnChangeDate;
        private System.Windows.Forms.ListBox lstOutput;
        private System.Windows.Forms.CheckBox cbxOverwrite;
    }
}

