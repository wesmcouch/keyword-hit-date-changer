﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace KeyworHitDateChanger
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<HitEvent> list = new List<HitEvent>();

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnChangeDate_Click(object sender, EventArgs e)
        {
            viewDate();
        }

        private void viewDate()
        {
            SqlConnection connectionString = new SqlConnection("Data Source=qf-sql2k8;Initial Catalog=WCouch_QF_Orca;Persist Security Info=True;User ID=sa;Password=sa");
                
            //dbConn.Open();

            SqlCommand cmd = new SqlCommand();
            //string selectStatement = "SELECT * FROM IR_Event WHERE RecordingId=" + txtRecordingID.Text;
            string selectStatement = "SELECT EventId, RecordingId, EventDate FROM WCouch_QF_Orca.dbo.IR_Event WHERE RecordingId=\'" +txtRecordingID.Text + "\'";
            cmd.CommandText = selectStatement;
            cmd.CommandType = CommandType.Text;
            cmd.Connection = connectionString;

            connectionString.Open();
            SqlDataReader reader;
            

            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                HitEvent readHit = new HitEvent();
                readHit.EventID = reader["EventId"].ToString();
                readHit.InteractionID = reader["RecordingId"].ToString();
                readHit.TimeOfInteraction = reader["EventDate"].ToString();

                string recordingID = readHit.InteractionID;
                lstOutput.Items.Add(readHit.EventID + "    " + readHit.InteractionID + "    " + readHit.TimeOfInteraction);
                
                if (cbxOverwrite.Checked == true)
                {
                    changeDate(readHit);
                }
            }
            connectionString.Close();

        }

        private void changeDate(HitEvent hitObject)
        {
            string[] splitDate = hitObject.TimeOfInteraction.Split(null);
            hitObject.TimeOfInteraction = txtNewDate.Text + " " + splitDate[1];

            SqlConnection connectionString = new SqlConnection("Data Source=qf-sql2k8;Initial Catalog=WCouch_QF_Orca;Persist Security Info=True;User ID=sa;Password=sa");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            string updateStatement = "UPDATE WCouch_QF_Orca.dbo.IR_Event SET EventDate = \'" + hitObject.TimeOfInteraction + "\' FROM WCouch_QF_Orca.dbo.IR_Event WHERE RecordingId=\'" + txtRecordingID.Text + "\' AND EventId=\'" + hitObject.EventID + "\'";
            cmd.CommandText = updateStatement;
            
            cmd.Connection = connectionString;
            connectionString.Open();
            cmd.ExecuteNonQuery();
            connectionString.Close();

        }
    }
}
